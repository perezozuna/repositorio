
package Controlador;

//import Controlador.ConexionBD;
import Modelo.Producto;
import Modelo.ProductoNoRefrigerado;
import Modelo.ProductoRefrigerado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ConsultasProducto extends ConexionBD{
    
    
    String sql;
    public Producto getProducto(String id){
        ConexionBD conexion = new ConexionBD();
        Producto objProducto= new Producto();
        sql="select * from Productos where id ='"+id+"' ;";
        System.out.println(""+sql);
        ResultSet resultSet = conexion.consultarBD(sql);
        try{
            if(resultSet.next()){
                double tempDB= resultSet.getDouble("temperatura");
                if(tempDB<=20){
                    objProducto=new ProductoRefrigerado();
                } else {
                    objProducto = new ProductoNoRefrigerado();
                }
                objProducto.setId(resultSet.getString("id"));
                objProducto.setNombre(resultSet.getString("nombre"));
                objProducto.setTemperatura(tempDB);
                objProducto.setValorBase(resultSet.getDouble("valorBase"));
                conexion.cerrarConexion();
            } else {
                conexion.cerrarConexion();
                System.out.println("No existe el producto con es id "+id);
                return null;
            }
        }catch(SQLException ex){
            System.out.println("consulta select error: "+ex.getMessage());
        }
        System.out.println(""+objProducto);
        return objProducto;
    }
    public List<Producto> listarProductos(){
        ConexionBD conexion = new ConexionBD();
        List<Producto> listaProductos = new ArrayList<>();
        String sql= "SELECT * FROM productos ;";//opcional order by temperatura desc
        ResultSet resultSet= conexion.consultarBD(sql);
        Producto objProducto;
        try {
            while(resultSet.next()){
                double tempDB= resultSet.getDouble("temperatura");
                if(tempDB<=20){
                    objProducto=new ProductoRefrigerado();
                } else {
                    objProducto = new ProductoNoRefrigerado();
                }
                objProducto.setId(resultSet.getString("id"));
                objProducto.setNombre(resultSet.getString("nombre"));
                objProducto.setTemperatura(tempDB);
                objProducto.setValorBase(resultSet.getDouble("valorBase"));
                listaProductos.add(objProducto);
            }
        } catch (SQLException e) {
            System.out.println("Error al consultar toda la tabla productos "+e.getSQLState());
        }
        conexion.cerrarConexion();
        return listaProductos;
    }
    
    public boolean agregarProducto(Producto p){
        ConexionBD conexion= new ConexionBD();
        sql="INSERT INTO productos (nombre,id,temperatura,valorBase)\n"
                + "values('"+p.getNombre()+ "', '"+p.getId()+ "',"
                + " "+p.getTemperatura()+ ","+p.getValorBase()+ " ); ";
        if(conexion.setAutoCommitBD(false)){
            if(conexion.insertarBD(sql)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    public boolean actualizarProducto(Producto p){
        ConexionBD conexion = new ConexionBD();
        sql ="update productos set "
                + "nombre='"+p.getNombre()+"', temperatura = "+p.getTemperatura()+","
                + " valorBase= "+p.getValorBase()+" where id ='"+p.getId()+ "' ; ";
        if(conexion.setAutoCommitBD(false)){
            if(conexion.insertarBD(sql)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    public boolean eliminarProducto(String id){
        ConexionBD conexion= new ConexionBD();
        String sentencia ="DELETE FROM productos where id= '"+id+"';" ; 
        if(conexion.setAutoCommitBD(false)){
            if(conexion.insertarBD(sentencia)){
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
                conexion.cerrarConexion();
                return false;
        }
    }
    
    
    
}
