
package Modelo;



public class ProductoNoRefrigerado extends Producto {

    public ProductoNoRefrigerado() {
    }

    public ProductoNoRefrigerado(String nombre, String id, double temperatura, double valorBase) {
        super(nombre, id, temperatura, valorBase);
    }

    @Override
    public double calcularCostoDeAlmacenamiento() {
        double calculoCosto;
        calculoCosto= getValorBase()*0.1+getValorBase();
        return calculoCosto;//getValorBase()*1.1;
    } 

//    @Override
//    public String nuevoMetAbstracto() {
//        return "Metodo abstracto de producto, sobreescrito por la sublace producto no refrigerado";
//    }

     
   
}
