
package Modelo;


public class Almacen {
    
    private String nombre;
    private String codigo;
    private final Lote lote;

    public Almacen( ) {
        this.lote = new Lote();
    }
    
    public void agregarProducto(Producto p){
        lote.agregarProducto(p);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
}
