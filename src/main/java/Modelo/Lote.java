
package Modelo;

import java.util.ArrayList;
import java.util.List;


public class Lote {
    
    private List<Producto> productos;

    public Lote() {
        this.productos= new ArrayList<>();
    }
    
    public void agregarProducto(Producto producto){
        this.productos.add(producto);
    }
    
    public void mostrarProductos(){
        for (Producto producto : productos) {
            System.out.println(producto);
        }
    }
}
