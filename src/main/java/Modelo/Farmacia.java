
package Modelo;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Farmacia {
    
    private String nit;
    private String nombre;
    private String direccion;
    private List<Almacen> almacenes;

    public Farmacia() {
        this.almacenes = new ArrayList<>();
    }
    
    public void agregarAlmacen(Almacen a){
        almacenes.add(a);
    }
    
    public void agregarProducto(String codigoAlmacen, Producto p){
        for (Almacen almacen : almacenes) {
            if(almacen.getCodigo().equals(codigoAlmacen)){
                almacen.agregarProducto(p);
            } else {
                System.out.println("Almacen no encontrado");
                
            }
        }
    }
    
}
